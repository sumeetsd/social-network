<!doctype html>
<html>
      <head>
            <title>Facebook: Login / Sign Up</title>
            <meta charset="utf-8" />
            <link ref="img/icon" rel="icon" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <link href="css/index.css" rel="stylesheet" />
            <link href="css/bootstrap.min.css" rel="stylesheet" />
            <link href="css/font-awesome.min.css" rel="stylesheet" />
            <script src="js/respond.js"></script>
      </head>
      <body id="bbooddyy">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                  <div class="container-fluid">
                        <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav_main" aria-expanded="false">
                                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                              </button>
                              <a class="navbar navbar-brand" href="#" style="margin-bottom:0px; font-size: 30px;">Facebook</a>
                        </div>
                        <div class="collapse navbar-collapse" id="nav_main" style="font-family:Arial Rounded Mt">
                              <form class="navbar-form navbar-right">
                                    <div class="form-group">
                                          <label for="login_id">Email or Phone</label>
                                          <input type="text" name="" id="login_id" class="form-control">
                                          <label for="login_pwd">Password</label>
                                          <input type="password" name="" id="login_pwd" class="form-control">
                                          <button type="submit" class="btn btn-primary btn-sm" style="width: 120px"> Login</button>
                                    </div>
                              </form>
                  </div>
            </nav>
            <div class="jumbotron" id="jumbo1">
            <div class="container">
            <div class="row">
                  <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12" id="basicinfo">
                        <div class="container">
                              <h2>Connect with friends and the <br />world around you on Facebook.</h2>
                              <div class="glyphicon glyphicon-"></div>
                        </div>
                  </div>
                  <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" id="signup">
                  <div class="container">
                  <form name="form_login" action="index.php">
                        <h2>Sign Up</h2>
                        <div class="input-group-md">
                              <input type="text" placeholder="First Name" id="input_data" size="18" />&nbsp;
                              <input type="text" placeholder="Last Name" id="input_data" size="18" />&nbsp;
                              <input type="text" placeholder="Mobile Number Or Email" id="input_data" size="40"/>&nbsp;
                              <input type="text" placeholder="Re-enter mobile number or email" id="input_data" size="40" />
                              &nbsp;<input type="password" placeholder="New Password" id="input_data" />&nbsp;
                        </div>

                        <label for="input_date">Birthday</label>
                        <div class="input-group">
                              <input type="date" name="date_of_birth" class="form-control" id="input_date" style="max-width: 200px">
                              <span style="font-size: 12px; margin-left: 10px"><a href="#">Why do I need to provide my birthday?</a></span> <br />
                        </div>

                        <div class="input-group">
                              <label for="gender_male">Male</label><label for="gender_female">Female</label>
                              <input type="radio" class="form-control" name="gender_male" value="Male" checked id="input_data" />
                              <input type="radio" class="form-control" name="gender_female" value="Female" id="input_data" />
                        </div>

                        <p>By clicking on Sign Up you agree to our <a href="#">Terms</a> and you have <br /> read our <a href="#"> data policy, </a>including our <a href="#">Cookie Use </a></p>

                        <button type="submit" class="btn btn-success" style="width: 200px; height: 35px; padding-top: 0px"> Sign Up </button>
                  </form>
                  </div>
                  </div>
            </div>
            </div>
            </div>
            <script src="jquery/jq_1_12.js"></script>
            <script src="js/bootstrap.min.js" type="text/javascript"></script>
      </body>
</html>
